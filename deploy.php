<?php
require_once 'recipe/common.php';

server('production', '88.208.238.235', 22)
	->path('/var/vhosts/jonathandeaves')
	->user('support')
	->pubKey();

set('shared_dirs',['storage']);
set('shared_files',[]);
set('repository', 'git@github.com:jondeaves/jonathandeaves.git');
set('writeable_dirs', ['app/storage']);

task('deploy:database', function() {
	run("php current/artisan migrate");
});


task('deploy:cache', function(){
	run("php current/artisan config:cache");
	run("php current/artisan route:cache");
});


task('deploy', [
	'deploy:start',
	'deploy:prepare',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writeable',
	'deploy:vendors',
	'deploy:symlink',
	'deploy:database',
	'deploy:cache',
	'cleanup',
	'deploy:end'
])->desc('Deploy your project');