@extends('layouts.app')
@section('metaTitle', 'Splat GGJ15 - '.Lang::get('app.websiteTitle'))@stop

@section('styles')
@parent
<link href="/css/splat.css" rel="stylesheet">
@stop

@section('content')

    <div class="content">

        <p>Made in 48 hours as part of Global Game Jam 2015.</p>

        <div id="unityPlayer">
            <div class="missing">
                <a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now!">
                    <img alt="Unity Web Player. Install now!" src="http://webplayer.unity3d.com/installation/getunity.png" width="193" height="63" />
                </a>
            </div>
            <div class="broken">
                <a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now! Restart your browser after install.">
                    <img alt="Unity Web Player. Install now! Restart your browser after install." src="http://webplayer.unity3d.com/installation/getunityrestart.png" width="193" height="63" />
                </a>
            </div>
        </div>
    </div>

@stop

@section('javascript')
@parent
<script src="/js/unityobject.js"></script>
<script src="/js/splat.js"></script>
@stop
