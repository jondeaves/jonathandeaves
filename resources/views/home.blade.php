@extends('layouts.app')

@section('content')

	<div class="page-container">

		<img src="img/me.jpg" alt="Jonathan Deaves" class="img-thumbnail img-responsive">

		<div class="social-block text-center">
			<a href="https://github.com/jondeaves" target="_blank"><i class="fa fa-github"></i></a>
			<a href="https://twitter.com/jondeaves" target="_blank"><i class="fa fa-twitter-square"></i></a>
			<a href="https://www.google.com/+JonathanDeaves" target="_blank"><i class="fa fa-google-plus-square"></i></a>
			<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#106;&#46;&#100;&#101;&#97;&#118;&#101;&#115;&#49;&#56;&#64;&#103;&#111;&#111;&#103;&#108;&#101;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;"><i class="fa fa-envelope"></i></a>

			<br>

			<a href="http://www.58gamez.com/save-the-bakon/" class="ggj14-image" target="_blank">
				<img src="games/bakon/ggj_logo.png" alt="Save the Bakon" title="Save the Bakon - Global Game Jam 2014" style="width:150px;">
			</a>

			<a href="{{ route('games.splat') }}" class="ggj15-image" target="_blank">
				<img src="games/splat/ggj_logo.png" alt="Splat" title="Splat - Global Game Jam 2015" style="width:200px;">
			</a>
		</div>
	</div>


@stop
